var personModule = angular.module('personApp', [ 'ngAnimate' ]);

personModule.controller('personController',
				function($scope, $http) {

					var urlBase = "";
					$scope.toggle = true;
					$scope.selection = [];
					$http.defaults.headers.post["Content-Type"] = "application/json";

					function findAllpersons() {
						$http.get(urlBase + '/persons')
								.success(
										function(data) {
											$scope.persons = data;
											for (var i = 0; i < $scope.persons.length; i++) {
													$scope.selection.push($scope.persons[i].personId);
											}
											$scope.personName = "";
											$scope.personAge = 0;
											$scope.toggle = '!toggle';
										});
					}

					findAllpersons();

					//add a new person
					$scope.addperson = function addperson() {
						if ($scope.personName == "" || $scope.personAge == "0") {
							alert("Insufficient Data! Please provide values for person name, age");
						} else {
							$http.post(urlBase + '/persons/insert/' +$scope.personName+'/'+$scope.personAge).
							    success(function(data) {
								    $scope.persons = data; 
								    $scope.personName="";
								    $scope.personAge="";  
									alert("person added");
									findAllpersons();
							      });
						}
					};

					$scope.toggleSelection = function toggleSelection(personUri) {
						var idx = $scope.selection.indexOf(personUri);
						//
					};

				});

//Angularjs Directive for confirm dialog box
personModule.directive('ngConfirmClick', [ function() {
	return {
		link : function(scope, element, attr) {
			var msg = attr.ngConfirmClick || "Are you sure?";
			var clickAction = attr.confirmedClick;
			element.bind('click', function(event) {
				if (window.confirm(msg)) {
					scope.$eval(clickAction);
				}
			});
		}
	};
} ]);