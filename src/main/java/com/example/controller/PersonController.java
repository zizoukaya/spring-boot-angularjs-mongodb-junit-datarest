package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.domain.Person;
import com.example.service.IPersonService;

@RestController
public class PersonController {

	@Autowired
	private IPersonService personService;

	@RequestMapping(value = "/persons", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<Person> getAllPersons() {
		List<Person> Persons = personService.getAllPersons();
		return Persons;
	}

	@RequestMapping(value = "/persons/insert/{personName}/{personAge}", method = RequestMethod.POST, headers = "Accept=application/json")
	public List<Person> addPerson(@PathVariable String personName, @PathVariable String personAge) {
		personService.addPerson(personName, personAge);
		return personService.getAllPersons();
	}
}