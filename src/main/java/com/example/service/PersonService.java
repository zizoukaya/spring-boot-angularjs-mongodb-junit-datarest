package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.PersonRepository;
import com.example.domain.Person;

@Service(value="personService")
public class PersonService implements IPersonService {

	@Autowired
	private PersonRepository personRepository;

	public Person addPerson(String personName, String personAge) {
		Person person = new Person();
		person.setName(personName);
		person.setAge(Integer.valueOf(personAge));
		return personRepository.save(person);
	}
	
	public List<Person> getAllPersons() {
		return (List<Person>) personRepository.findAll();
	}

}