package com.example.service;

import java.util.List;

import com.example.domain.Person;

public interface IPersonService {
	
	public Person addPerson(String personName, String personAge);
	public List<Person> getAllPersons();
	
}
