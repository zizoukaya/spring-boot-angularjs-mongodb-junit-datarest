package com.example.dao;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.domain.Person;

public interface PersonRepository extends CrudRepository<Person, Long> {
	@Query("{'name' : ?0}")
	public Iterable<Person> searchByName(String personName);

}
