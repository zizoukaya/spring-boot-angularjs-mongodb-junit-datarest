package com.example.service;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.example.BaseTest;
import com.example.dao.PersonRepository;
import com.example.domain.Person;

public class PersonServiceTest extends BaseTest {
	
	@InjectMocks
	private PersonService personService;
	
	@Mock
	private PersonRepository personRepository;

	private List<Person> personList = new ArrayList<Person>();
	private Person person1 = null;
	private Person person2 = null;
	private Person person3 = null;
	
	@Before
	public void setup() {
		person1 = new Person();
		person2 = new Person();
		person3 = new Person();
		
		person1.setName("ahmet");
		person1.setAge(27);
		
		person2.setName("kaya");
		person2.setAge(28);

		person3.setName("ahmetkaya");
		person3.setAge(29);

		personList.add(person1);
		personList.add(person2);
	}
	
	@Test
	public void shouldReturnPersonList() {
		when(personRepository.findAll()).thenReturn(personList);

		List<Person> returnList = personService.getAllPersons();
	
		Assert.assertEquals(returnList, personList);
		Assert.assertEquals(returnList.size(), personList.size());
		Assert.assertTrue(returnList.contains(person1));
		Assert.assertTrue(returnList.contains(person2));
		Assert.assertFalse(returnList.contains(person3));
	}

	@Test
	public void shouldAddNewPerson() {
		Person newPerson = new Person();
		String name = "new recruit";
		String age = "1";
		newPerson.setName(name);
		newPerson.setAge(Integer.valueOf(age));
		
		when(personRepository.save(any(Person.class))).thenReturn(newPerson);

		Person returnPerson = personService.addPerson(name, age);

		Assert.assertEquals(returnPerson, newPerson);
		Assert.assertEquals(returnPerson.getName(), newPerson.getName());
		Assert.assertEquals(returnPerson.getAge(), newPerson.getAge());
	}
}
