package com.example.controller;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;

import com.example.BaseTest;

public class HomeControllerTest extends BaseTest {
	
	@InjectMocks
	private HomeController homeController;
	
	@Test
	public void shouldReturnIndex() {
		String returnValue = homeController.home();
		Assert.assertEquals("index", returnValue);
	}

}
