package com.example.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.example.BaseTest;
import com.example.domain.Person;
import com.example.service.IPersonService;

public class PersonControllerTest extends BaseTest {
	
	@InjectMocks
	private PersonController personController;
	
	@Mock
	private IPersonService personService;

	private List<Person> personList = new ArrayList<Person>();
	private Person person1 = null;
	private Person person2 = null;
	private Person person3 = null;
	
	@Before
	public void setup() {
		person1 = new Person();
		person2 = new Person();
		person3 = new Person();
		
		person1.setName("ahmet");
		person1.setAge(27);
		
		person2.setName("kaya");
		person2.setAge(28);

		person3.setName("ahmetkaya");
		person3.setAge(29);

		personList.add(person1);
		personList.add(person2);
	}
	
	@Test
	public void shouldReturnPersonList() {
		when(personService.getAllPersons()).thenReturn(personList);

		List<Person> returnList = personController.getAllPersons();
	
		Assert.assertEquals(returnList, personList);
		Assert.assertEquals(returnList.size(), personList.size());
		Assert.assertTrue(returnList.contains(person1));
		Assert.assertTrue(returnList.contains(person2));
		Assert.assertFalse(returnList.contains(person3));
	}

	@Test
	public void shouldAddNewPerson() {
		when(personService.getAllPersons()).thenReturn(personList);

		String name = "new recruit";
		String age = "1";
		
		personController.addPerson(name, age);
		
		verify(personService).addPerson(name, age);
	}
}
